package com.thoughtworks.springbootemployee.exception;

public class EmployeeNotInTheCompanyException extends RuntimeException{
    public EmployeeNotInTheCompanyException(String message) {
        super(message);
    }
}
