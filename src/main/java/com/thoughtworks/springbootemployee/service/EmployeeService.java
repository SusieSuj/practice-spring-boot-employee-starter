package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.common.Gender;
import com.thoughtworks.springbootemployee.exception.AgeIsNotValidException;
import com.thoughtworks.springbootemployee.exception.AgeNotMatchSalaryException;

import com.thoughtworks.springbootemployee.exception.EmployeeNotInTheCompanyException;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeStorageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Service
@RequiredArgsConstructor
public class EmployeeService {
    List<Employee> employees = new ArrayList<>();
    private final AtomicLong atomicId = new AtomicLong(0);
    @Autowired
    private final EmployeeStorageRepository employeeStorageRepository;

    public List<Employee> queryCompanyEmployees(Long companyId) {
        return employeeStorageRepository.queryCompanyEmployees(companyId);
    }

    public Employee createEmployee(Employee employee) {
        if (!employee.isAgeValid(employee)) {
            throw new AgeIsNotValidException();
        }
        if (employee.ageNotMatchSalary(employee)) {
            throw new AgeNotMatchSalaryException();
        }
        employee.setActive(true);
        return employeeStorageRepository.createEmployee(employee);

    }

    public List<Employee> getEmployees() {
        return employeeStorageRepository.queryEmployees();
    }

    public Employee queryEmployeeById(Long id) {
        return employeeStorageRepository.queryEmployeeById(id);
    }

    public List<Employee> queryEmployeeByGender(Gender gender) {
        return employeeStorageRepository.queryEmployeeByGender(gender);
    }

    public Employee updateEmployeeAgeAndSalary(Long id, Employee employee) {
        Employee employeeUpdated = employeeStorageRepository.queryEmployeeById(id);
        if (employeeUpdated!=null && employeeUpdated.isActive())
            return employeeStorageRepository.updateEmployeeTemp(employeeUpdated, employee);
        throw new EmployeeNotInTheCompanyException("the employee had left the company");
    }

    public void deleteEmployee(Long id) {
        Employee employeeDeleted = employeeStorageRepository.queryEmployeeById(id);
        if (employeeDeleted.isActive()) {
            employeeDeleted.setActive(false);
            employeeStorageRepository.deleteEmployeeTemp(employeeDeleted);
        }


    }

    public List<Employee> queryEmployeePage(int page, int size) {
        return employeeStorageRepository.queryEmployeePage(page, size);
    }
}
