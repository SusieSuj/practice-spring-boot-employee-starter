package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.common.Gender;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeStorageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee createEmployee(@RequestBody Employee employee) {
       return employeeService.createEmployee(employee);
    }

    @GetMapping
    public List<Employee> getEmployees() {
        return employeeService.getEmployees();
    }

    @GetMapping("/{id}")
    public Employee queryEmployeeById(@PathVariable Long id) {
        return employeeService.queryEmployeeById(id);
    }


    @GetMapping(params = {"gender"})
    public List<Employee> queryEmployeeByGender(@RequestParam Gender gender) {
        return employeeService.queryEmployeeByGender(gender);

    }

    @PutMapping("/{id}")
    public Employee updateEmployeeAgeAndSalary(@PathVariable Long id, @RequestBody Employee employee) {
        return employeeService.updateEmployeeAgeAndSalary(id,employee);

    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable Long id) {
        employeeService.deleteEmployee(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> queryEmployeePage(@RequestParam int page, int size) {
        return employeeService.queryEmployeePage(page,size);
    }

}
