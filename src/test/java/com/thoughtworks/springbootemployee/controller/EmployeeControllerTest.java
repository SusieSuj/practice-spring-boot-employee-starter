package com.thoughtworks.springbootemployee.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.common.Gender;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeStorageRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerTest {
    @Autowired
    private EmployeeStorageRepository employeeRepository;

    @Autowired
    MockMvc client;

    @BeforeEach
    void cleanEmployeeData() {
        employeeRepository.clearAll();
    }

    @Test
    void should_return_employees_when_getAllEmployees_given_employees() throws Exception {
//        given
//        Employee john = new Employee(null, "John", 32, "Male", 5000.0);
        Employee john = Employee.builder().name("John").age(32).gender(Gender.MALE).salary(5000.0).build();
        employeeRepository.createEmployee(john);
//        when
        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(john.getId()))
                .andExpect(jsonPath("$[0].name").value(john.getName()))
                .andExpect(jsonPath("$[0].age").value(john.getAge()))
                .andExpect(jsonPath("$[0].gender").value("Male"))
                .andExpect(jsonPath("$[0].salary").value(john.getSalary()));
    }

    @Test
    void should_return_employees_with_gender_when_perform_findEmployeesByGender_given_2_employees_with_different_gender() throws Exception {
        Employee john = Employee.builder().name("John").age(32).gender(Gender.MALE).salary(5000.0).build();
        Employee may = Employee.builder().name("May").age(22).gender(Gender.FEMALE).salary(6000.0).build();
        employeeRepository.createEmployee(john);
        employeeRepository.createEmployee(may);
        client.perform(MockMvcRequestBuilders.get("/employees").param("gender", String.valueOf(Gender.FEMALE)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(may.getId()))
                .andExpect(jsonPath("$[0].name").value(may.getName()))
                .andExpect(jsonPath("$[0].age").value(may.getAge()))
                .andExpect(jsonPath("$[0].gender").value("Female"))
                .andExpect(jsonPath("$[0].salary").value(may.getSalary()));
    }

    @Test
    void should_return_created_employee_when_insertEmployee_given_employee_json() throws Exception {
        String johnSmith = "{\n" +
                "    \"name\":\"John Smith\",\n" +
                "    \"age\":22,\n" +
                "    \"gender\":\"Male\",\n" +
                "    \"salary\":9000\n" +
                "}";
//        Employee john = new Employee(null, "John", 32, "Male", 5000.0);
//       String john = new ObjectMapper().writeValueAsString(johnSmith);

        client.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(johnSmith))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("John Smith"))
                .andExpect(jsonPath("$.age").value(22))
                .andExpect(jsonPath("$.gender").value("Male"))
                .andExpect(jsonPath("$.salary").value(9000));
    }


    @Test
    void should_return_employee_when_getEmployeeById_given_employeeId() throws Exception {
        Employee john = Employee.builder().id(1L).name("John").age(20).gender(Gender.MALE).salary(5000.0).build();
        employeeRepository.createEmployee(john);
        client.perform(MockMvcRequestBuilders.get("/employees/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value(john.getName()))
                .andExpect(jsonPath("$.age").value(john.getAge()))
                .andExpect(jsonPath("$.gender").value("Male"))
                .andExpect(jsonPath("$.salary").value(john.getSalary()));
    }

    @Test
    void should_updated_employee_when_updateEmployee_given_employee() throws Exception {
        Employee johnSmith = Employee.builder().id(1L).name("John").age(32).gender(Gender.MALE).salary(5000.0).build();
        Employee johnSmithUpdate = Employee.builder().id(1L).name("John").age(36).gender(Gender.MALE).salary(9000.0).build();
        String johnSmithUpdateJson = new ObjectMapper().writeValueAsString(johnSmithUpdate);
        employeeRepository.createEmployee(johnSmith);
        client.perform(MockMvcRequestBuilders.put("/employees/1").contentType(MediaType.APPLICATION_JSON).content(johnSmithUpdateJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value(johnSmithUpdate.getName()))
                .andExpect(jsonPath("$.age").value(johnSmithUpdate.getAge()))
                .andExpect(jsonPath("$.gender").value("Male"))
                .andExpect(jsonPath("$.salary").value(johnSmithUpdate.getSalary()));
    }

    @Test
    void should_delete_employee_when_deleteEmployee_given_id() throws Exception {
// given
        Employee aimy = Employee.builder().id(1L).name("aimy").age(33).gender(Gender.FEMALE).salary(60000.0).build();
        employeeRepository.createEmployee(aimy);
// then
        client.perform(MockMvcRequestBuilders.delete("/employees/{id}", 1))
                .andExpect(status().isNoContent());
    }

    @Test
    void should_return_a_page_of_employees_when_getOnePageOfEmployees_given_page_and_size() throws Exception {
// given
        Employee aimy = Employee.builder().id(1L).name("aimy").age(33).gender(Gender.FEMALE).salary(60000.0).build();
        employeeRepository.createEmployee(aimy);
        for (int i = 2; i < 10; i++) {
            Employee tmpEmployee = Employee.builder().name("aimy" + i).age(33).gender(Gender.FEMALE).salary(60000.0).build();
            employeeRepository.createEmployee(tmpEmployee);
        }

// then
        client.perform(MockMvcRequestBuilders.get("/employees")
                        .param("size", "3")
                        .param("page", "1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").value("aimy"))
                .andExpect(jsonPath("$[0].age").value(33))
                .andExpect(jsonPath("$[0].gender").value("Female"))
                .andExpect(jsonPath("$[0].salary").value(60000.0));
    }


}
