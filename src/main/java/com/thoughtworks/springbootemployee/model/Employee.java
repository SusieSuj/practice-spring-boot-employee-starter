package com.thoughtworks.springbootemployee.model;

import com.thoughtworks.springbootemployee.common.Gender;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Employee {
    private Long id;
    private final String name;
    private Integer age;
    private Gender gender;
    private Double salary;
    private final Long companyId;
    private boolean active;

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isAgeValid(Employee employee) {
        return employee.getAge()>=18 && employee.getAge()<=65;
    }

    public boolean ageNotMatchSalary(Employee employee) {
        return employee.getAge()>=30 && employee.getSalary()<20000;
    }

}
