package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.common.Gender;
import com.thoughtworks.springbootemployee.exception.AgeIsNotValidException;
import com.thoughtworks.springbootemployee.exception.AgeNotMatchSalaryException;
import com.thoughtworks.springbootemployee.exception.EmployeeNotInTheCompanyException;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeStorageRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class EmployeeServiceTest {
    @Test
    void should_not_create_successful_when_create_employee_given_age_is_invalid() {
        EmployeeService employeeService = new EmployeeService(mock(EmployeeStorageRepository.class));
        Employee employee = Employee.builder().id(1L).name("sj").age(15).salary(6000.0).companyId(1L).build();
        assertThrows(AgeIsNotValidException.class, () -> employeeService.createEmployee(employee));
    }

    @Test
    void should_not_create_successful_when_create_employee_age_not_match_salary() {
        EmployeeService employeeService = new EmployeeService(mock(EmployeeStorageRepository.class));
        Employee employee = Employee.builder().id(1L).name("sj").age(32).salary(19000.0).companyId(1L).build();
        assertThrows(AgeNotMatchSalaryException.class, () -> employeeService.createEmployee(employee));
    }


    @Test
    void should_create_successful_when_create_employee_given_age_is_valid() {
        EmployeeStorageRepository mockEmployeeRepository = mock(EmployeeStorageRepository.class);
        EmployeeService employeeService = new EmployeeService(mockEmployeeRepository);
        Employee employee = Employee.builder().id(1L).name("sj").age(20).salary(6000.0).companyId(1L).build();

        given(mockEmployeeRepository.createEmployee(any())).willReturn(employee);

        employeeService.createEmployee(employee);
        verify(mockEmployeeRepository).createEmployee(argThat(employeeCreated->{
            assertThat(employeeCreated.getName()).isEqualTo(employee.getName());
            assertThat(employeeCreated.getAge()).isEqualTo(employee.getAge());
            assertThat(employeeCreated.getGender()).isEqualTo(employee.getGender());
            assertThat(employeeCreated.getSalary()).isEqualTo(employee.getSalary());
            return true;
        }));

    }

    @Test
    void should_set_in_active_when_delete_employee_given_an_active_employee() {
        EmployeeStorageRepository mockEmployeeRepository = mock(EmployeeStorageRepository.class);
        EmployeeService employeeService = new EmployeeService(mockEmployeeRepository);
        Employee employee = Employee.builder().id(1L).name("sj").age(20).salary(6000.0).companyId(1L).build();
        employee.setActive(true);

        given(mockEmployeeRepository.queryEmployeeById(1L)).willReturn(employee);

        employeeService.deleteEmployee(1L);
        verify(mockEmployeeRepository,times(1)).deleteEmployeeTemp(employee);
        assertFalse(employee.isActive());
    }

    @Test
    void should_set_in_active_when_create_a_new_employee(){
        EmployeeStorageRepository mockEmployeeRepository = mock(EmployeeStorageRepository.class);
        EmployeeService employeeService = new EmployeeService(mockEmployeeRepository);
        Employee employee = Employee.builder().id(1L).name("sj").age(20).salary(6000.0).companyId(1L).build();

        given(mockEmployeeRepository.createEmployee(employee)).willReturn(employee);

        employeeService.createEmployee(employee);
        verify(mockEmployeeRepository,times(1)).createEmployee(employee);
        assertTrue(employee.isActive());
    }

    @Test
    void should_not_update_employee_when_updateEmployeeAgeAndSalary_given_the_employee_is_left(){
        EmployeeStorageRepository mockEmployeeRepository = mock(EmployeeStorageRepository.class);
        EmployeeService employeeService = new EmployeeService(mockEmployeeRepository);
        Employee employee = Employee.builder().id(1L).name("sj").age(20).salary(6000.0).companyId(1L).gender(Gender.FEMALE).build();
        Employee employeeUpdated = Employee.builder().id(1L).name("sj").age(22).salary(8000.0).companyId(1L).gender(Gender.FEMALE).build();
        employee.setActive(false);

        given(mockEmployeeRepository.updateEmployeeTemp(employeeUpdated,employee)).willReturn(employeeUpdated);

        EmployeeNotInTheCompanyException employeeNotInTheCompanyException = Assertions.assertThrows(EmployeeNotInTheCompanyException.class,
                ()->employeeService.updateEmployeeAgeAndSalary(1L,employeeUpdated));
        Assertions.assertEquals("the employee had left the company",employeeNotInTheCompanyException.getMessage());


    }




}
