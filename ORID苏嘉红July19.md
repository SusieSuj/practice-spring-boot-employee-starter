

# O

- The topic of today's study is: Spring boot
- Today I learned to test in the Spring Boot project, and some spring annotations. For example, @Data annotation can help us create the constructor and getter and setter methods. When we use the annatation @RequiredArgsConstructor, the properties should be created final. 

# R

- I think the assertions are a little complex.

# I

- I encountered a problem while doing the exercise: a test is passed when I only run itself, but failed when I do batch testing. After investigation, I  found that it was because when doing batch testing, the variable :**private final AtomicLong atomicId = new AtomicLong(0);**  may not always start at 0. I add the code: **atomicId.set(0);** in clearAll method, and then the tests all passed. 

# D

- I don't baby commit when I just doing the exercise. I should develop the habit of baby commit even I am not doing the homework.

