package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.exception.CompanyNotFoundException;
import com.thoughtworks.springbootemployee.model.Company;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

//@Component
@Repository
public class CompanyStorageRepository {
    private final List<Company> companies = new ArrayList<>();
    private final AtomicLong atomicId = new AtomicLong(0);


    public Company createCompany(Company company) {
        company.setId(atomicId.incrementAndGet());
        companies.add(company);
        return company;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public Company queryCompanyById(Long id) {
        return companies.stream()
                .filter(company -> Objects.equals(company.getId(), id))
                .findFirst()
                .orElseThrow(CompanyNotFoundException::new);
    }

    public List<Company> queryCompanyPage(int page, int size) {
        return companies.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Company updateCompanyNameTemp(Company companyUpdated, Company company) {
        companies.remove(companyUpdated);
        companies.add(company);
        return company;

    }

    public void deleteCompanyTemp(Company companyDeleted) {
        companies.remove(companyDeleted);
    }

    public void clearAll() {
        companies.clear();
        atomicId.set(0);
    }
}
