package com.thoughtworks.springbootemployee.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Company {
    private Long id;
    private String name;

}
