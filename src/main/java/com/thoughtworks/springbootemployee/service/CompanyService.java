package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.exception.CompanyNotFoundException;
import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.repository.CompanyStorageRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeStorageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    @Autowired
    private CompanyStorageRepository companyStorageRepository;
    @Autowired
    private EmployeeStorageRepository employeeStorageRepository;



    public Company createCompany(Company company) {
       return companyStorageRepository.createCompany(company);
    }

    public List<Company> getCompanies() {
        return companyStorageRepository.getCompanies();
    }

    public Company queryCompanyById(Long id) {
        return companyStorageRepository.queryCompanyById(id);
    }

    public Company updateCompanyName(Long id, Company company){
        Company companyUpdated = companyStorageRepository.queryCompanyById(id);
        if (companyUpdated==null)
            throw new CompanyNotFoundException();
        return companyStorageRepository.updateCompanyNameTemp(companyUpdated, company);
    }

    public void deleteCompany(Long id) {
        Company companyDeleted = companyStorageRepository.queryCompanyById(id);
        if (companyDeleted==null)
            throw new CompanyNotFoundException();
        companyStorageRepository.deleteCompanyTemp(companyDeleted);
    }

    public List<Company> queryCompanyPage(int page, int size) {
        return companyStorageRepository.queryCompanyPage(page, size);
    }
}
