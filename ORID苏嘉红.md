# O

- The topic of today's study is: restful API design, HTTP, Spring boot
- The restful API design requires us to be prescriptive when writing interfaces. For example, nouns denoting resources should use the plural. I also learned how to represent nested resources and pass parameters. 
- The HTTP section focuses on common methods and response states. For example, we can use @ResponseStatus(HttpStatus.*CREATED*) to return a 201 status. 
- We learned to use Spring Boot to receive and process requests from the web. Spring boot is more  concise compared  to spring. I don't need to think about dependencies between jar packages, which may lead to a lot of conflicts. 

# R

- I'm not very familiar with the use of annotations.

# I

- We need to use the HTTP status code to mark the return information.

# D

- I need to learn the use of spring and spring boot annotations.