package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.common.Gender;
import com.thoughtworks.springbootemployee.exception.EmployeeNotFoundException;
import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class EmployeeStorageRepository {
    List<Employee> employees = new ArrayList<>();

    private final AtomicLong atomicId = new AtomicLong(0);

    public Employee createEmployee(Employee employee) {
        employee.setActive(true);
        employee.setId(atomicId.incrementAndGet());
        employees.add(employee);
        return employee;
    }

    public List<Employee> queryEmployees() {
        return employees;
    }

    public Employee queryEmployeeById(Long id) {
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getId(), id))
                .findFirst()
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public List<Employee> queryEmployeeByGender(Gender gender) {
        return employees.stream().
                filter(employee -> Objects.equals(employee.getGender(), gender))
                .collect(Collectors.toList());
    }


    public List<Employee> queryEmployeePage(int page, int size) {
        return employees.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public List<Employee> queryCompanyEmployees(Long companyId) {
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getCompanyId(), companyId))
                .collect(Collectors.toList());
    }

    public void deleteEmployeeTemp(Employee employee) {
        Employee employeeDeleted = employees.stream().filter(employeeItem -> employeeItem.getId().equals(employee.getId())).findFirst().orElseThrow(EmployeeNotFoundException::new);
        employees.remove(employeeDeleted);
        employees.add(employee);

    }

    public Employee updateEmployeeTemp(Employee employee, Employee employeeUpdated) {
        employees.remove(employee);
        employees.add(employeeUpdated);
        return employeeUpdated;
    }

    public void clearAll() {
        employees.clear();
        atomicId.set(0);
    }
}
