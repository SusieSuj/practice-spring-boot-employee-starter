package com.thoughtworks.springbootemployee.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.common.Gender;
import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyStorageRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeStorageRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {

    @Autowired
    private CompanyStorageRepository companyStorageRepository;
    @Autowired
    private EmployeeStorageRepository employeeStorageRepository;

    @Autowired
    MockMvc client;

    @BeforeEach
    void cleanCompanyData() {
        companyStorageRepository.clearAll();
    }

    @Test
    void should_return_companies_when_getCompanies_given_employees() throws Exception {
        Company company1 = Company.builder().id(1L).name("company1").build();
        Company company2 = Company.builder().id(2L).name("company2").build();

        companyStorageRepository.createCompany(company1);
        companyStorageRepository.createCompany(company2);

        client.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(company1.getId()))
                .andExpect(jsonPath("$[0].name").value(company1.getName()))
                .andExpect(jsonPath("$[1].id").value(company2.getId()))
                .andExpect(jsonPath("$[1].name").value(company2.getName()));
    }

    @Test
    void should_return_created_company_when_createCompany_given_a_company() throws Exception {
        Company company = Company.builder().id(1L).name("company1").build();
        String companyJson = new ObjectMapper().writeValueAsString(company);
        client.perform(MockMvcRequestBuilders.post("/companies")
                .contentType(MediaType.APPLICATION_JSON)
                .content(companyJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(company.getId()))
                .andExpect(jsonPath("$.name").value(company.getName()));
    }

    @Test
    void should_return_company_when_queryCompanyById_given_companyId() throws Exception {
        Company company = Company.builder().id(1L).name("company1").build();
        companyStorageRepository.createCompany(company);
        client.perform(MockMvcRequestBuilders.get("/companies/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value(company.getName()));
    }


    @Test
    void should_return_updated_company_when_updateCompanyName_given_companyId_and_update_company() throws Exception {
        Company company = Company.builder().id(1L).name("company1").build();
        companyStorageRepository.createCompany(company);
        Company companyUpdated = Company.builder().id(1L).name("company2").build();
        String companyUpdatedJson = new ObjectMapper().writeValueAsString(companyUpdated);
        client.perform(MockMvcRequestBuilders.put("/companies/1").contentType(MediaType.APPLICATION_JSON).content(companyUpdatedJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value(companyUpdated.getName()));
    }
    @Test
    void should_delete_company_when_deleteCompany_given_id() throws Exception {
        Company company = Company.builder().id(1L).name("company1").build();
        companyStorageRepository.createCompany(company);
        client.perform(MockMvcRequestBuilders.delete("/companies/{id}", 1))
                .andExpect(status().isNoContent());
    }

    @Test
    void should_return_a_page_of_companies_when_queryCompanyPage_given_page_and_size() throws Exception {
// given
        Company company = Company.builder().id(1L).name("company").build();
        companyStorageRepository.createCompany(company);
        for (int i = 1; i < 10; i++) {
            Company companyItem = Company.builder().id(1L+i).name("company"+i).build();
            companyStorageRepository.createCompany(companyItem);
        }

// then
        client.perform(MockMvcRequestBuilders.get("/companies")
                        .param("size", "3")
                        .param("page", "1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").value("company"))
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].name").value("company1"))
                .andExpect(jsonPath("$[2].id").value(3))
                .andExpect(jsonPath("$[2].name").value("company2"));
    }

    @Test
    void should_return_the_employee_in_company1_when_queryCompanyEmployees_given_employees_in_2_companies() throws Exception {
        Company company1 = Company.builder().id(1L).name("company1").build();
        Company company2 = Company.builder().id(2L).name("company2").build();
        companyStorageRepository.createCompany(company1);
        companyStorageRepository.createCompany(company2);

        Employee aimy = Employee.builder().id(1L).name("aimy").age(23).gender(Gender.FEMALE).salary(60000.0).companyId(1L).build();
        Employee john = Employee.builder().id(2L).name("john").age(33).gender(Gender.MALE).salary(60000.0).companyId(2L).build();

        employeeStorageRepository.createEmployee(aimy);
        employeeStorageRepository.createEmployee(john);

        client.perform(MockMvcRequestBuilders.get("/companies/1/employees")
                        .param("companyId", "1"))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").value("aimy"))
                .andExpect(jsonPath("$[0].age").value(23))
                .andExpect(jsonPath("$[0].gender").value("Female"))
                .andExpect(jsonPath("$[0].salary").value(60000.0));

    }

}
