package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.service.CompanyService;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyStorageRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeStorageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping("/companies")
@RequiredArgsConstructor
public class CompanyController {

    private final CompanyStorageRepository companyStorageRepository;
    private final EmployeeStorageRepository employeeStorageRepository;

    private final CompanyService companyService;
    private final EmployeeService employeeService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company createCompany(@RequestBody Company company) {
       return companyService.createCompany(company);
    }

    @GetMapping
    public List<Company> getCompanies() {
        return companyService.getCompanies();

    }

    @GetMapping("/{id}")
    public Company queryCompanyById(@PathVariable Long id) {
        return companyService.queryCompanyById(id);

    }

    @PutMapping("/{id}")
    public Company updateCompanyName(@PathVariable Long id, @RequestBody Company company) {
        return companyService.updateCompanyName(id,company);

    }


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id) {
        companyService.deleteCompany(id);

    }

    @GetMapping(params = {"page", "size"})
    public List<Company> queryCompanyPage(@RequestParam int page, int size) {
        return companyService.queryCompanyPage(page,size);

    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> queryCompanyEmployees(@PathVariable Long companyId) {
        return employeeService.queryCompanyEmployees(companyId);
    }
}
